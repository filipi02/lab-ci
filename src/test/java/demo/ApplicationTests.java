package demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTests {
	
	@Autowired SentenceService sentenceService;

	@Test
	public void testSentence() {
		final String sentence = sentenceService.getSentence("test");
		
		assertEquals(sentence, "Your active profile is test");		
	}
	
	@Test
	public void failTestSentence() {
		final String sentence = sentenceService.getSentence("test");
		
		assertEquals(sentence, "fail test");		
	}

}
