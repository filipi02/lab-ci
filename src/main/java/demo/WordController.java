package demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WordController {

	@Value("${words}") String words;
	
	@Autowired SentenceService sentenceService;
	
	@GetMapping("/")
	public @ResponseBody String getWord() {
		return sentenceService.getSentence(words);
	}
}
