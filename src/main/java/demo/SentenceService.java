package demo;

import org.springframework.stereotype.Service;

@Service
public class SentenceService {
	
	public String getSentence(final String profile) {
		return "Your active profile is " + profile;
	}

}
